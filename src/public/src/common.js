const MAX_FTE_SCROLL_INTERVAL_TIME = 5000;

$(window).on('action:ajaxify.start', () => {
  console.log('fte: action:ajaxify.start');
  window.removeEventListener('scroll', fteScrollHandler);
});

$(window).on('action:ajaxify.end', () => {
  console.log('fte: action:ajaxify.end');
  if (['/', '/news'].includes(window.location.pathname)) {
    init();
  }
});

let listEl = null
let nextPage;

function init () {
  nextPage = 2;
  const timeStart = new Date();

  const intervalId = setInterval(() => {
    if (new Date() - timeStart > MAX_FTE_SCROLL_INTERVAL_TIME) clearInterval(intervalId);
    listEl = document.querySelector(".fte-news");
    if (listEl) {
      clearInterval(intervalId);
      window.addEventListener('scroll', fteScrollHandler);
    }
  }, 10);
}

function fteScrollHandler() {
  if (nextPage && (window.scrollY + window.innerHeight >= listEl.scrollHeight - 100)) getNews();
}

async function getNews () {
  try {
    window.removeEventListener('scroll', fteScrollHandler)
    showSpinner();

    let response = await $.get('/api/news/' + nextPage);
    $('.fte-news').append(response.featuredTemplate);

    nextPage = response.nextpage;
  } catch (e) {
    console.error(e);
  } finally {
    hideSpinner();
    window.addEventListener('scroll', fteScrollHandler)
  }
}

function showSpinner () {
  $('.spinner-overlay').css('visibility', 'visible');
  $('html, body').css('overflow', 'hidden');
}

function hideSpinner () {
  $('.spinner-overlay').css('visibility', 'hidden');
  $('html, body').css('overflow', 'auto');
}
